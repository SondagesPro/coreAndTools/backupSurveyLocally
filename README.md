# backupSurveyLocally

Backup survey on the server as lsa or lss file.

Plugin tested on LimeSurvey 3 and 5.6.65. 

## Usage

- Create a directory outside web (else everyone can get your backuped file)
- Set plugin configuration
    - Set complete directory name to _Local directory_
    - Set the number of backup to keep
- Go to each survey you want to be backuped
- Plugin settings / backupSurveyLocally
- Check _Backup survey locally._
- Alternatively : you can add a list of surveys id in your _Local directory_ name surveys.txt. Each survey id separated by new line
- Save your survey locally with PHP Cli : <code>php application/commands/console.php plugin --target=backupSurveyLocally</code>
- User can get the list of backup file and download in the tool menu

If you use LimeSurvey version before 3.17.8, you need to be in LimeSurvey directory for call console.php.

Then for a cron script :
````
#!/bin/bash
cd /var/www/limesurvey
php application/commands/console.php plugin --target=backupSurveyLocally
````

## Home page, copyright and support
- HomePage <https://extensions.sondages.pro/>
- Copyright © 2019-2021 Denis Chenu <https://sondages.pro> and [contributors](https://gitlab.com/SondagesPro/coreAndTools/backupSurveyLocally/-/graphs/master)
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- Merge request on [Gitlab](https://gitlab.com/SondagesPro/coreAndTools/backupSurveyLocally/merge_requests)
- Issues on [Gitlab](https://gitlab.com/SondagesPro/coreAndTools/backupSurveyLocally/issues)
- [Professional support](https://support.sondages.pro/)
