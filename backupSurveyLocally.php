<?php

/**
 * backupSurveyLocally : Backup your survey locally.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2024 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 *
 * @version 1.5.3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class backupSurveyLocally extends PluginBase
{
    // @inheritdoc
    public $allowedPublicMethods = [
        'actionList',
        'actionGet',
    ];
    protected $storage = 'DbStorage';
    protected static $description = 'Backup your survey locally';
    protected static $name = 'backupSurveyLocally';

    /**
     * @var array[] the settings
     */
    protected $settings = [
        'information' => [
            'type' => 'info',
            'content' => '<div class="well">'
                    . '<p>You need activate cron system in the server : <code>php ./application/commands/console.php plugin index --target=backupSurveyLocally</code>.</p>'
                    . '</div>',
        ],
        'localDirectory' => [
            'type' => 'string',
            'label' => 'Local directory where backup your survey',
            'help' => 'This directory must be out of web access, but web user must be allowed to create directory and write in this directory',
        ],
        'backupNumber' => [
            'type' => 'int',
            'label' => 'Number of backups',
            'htmlOptions' => [
                'min' => 1,
            ],
            'help' => 'The last backups are in directory name «last», each previous are backup-2, backup-3 … until the number here.',
            'default' => 7,
        ],
        'setMemoryLimit' => [
            'type' => 'int',
            'label' => 'Memory limit',
            'help' => 'Memory limit to set, update this can be disable on some server. To have no memory limit, set this directive to -1.',
            'htmlOptions' => array(
                'min' => -1
            ),
        ],
        'disableErrorReporting' => [
            'type' => 'boolean',
            'label' => 'Disable error when save surveys',
            'help' => 'If a survey have an error, this broke whole other survey to be saved. Disable error reporting allow to continue.',
            'default' => true,
        ],
        'setChmod' => [
            'type' => 'boolean',
            'label' => 'Update permission on each file and directiry after creation',
            'help' => 'If multiple user must have access to file and directory : you can force permission to all read, write for file and read, write and execute for directory.',
            'default' => false,
        ],
    ];

    // @var string
    private $_errorOnDir = '';

    // @var null|integer
    private $originalErrorReporting = null;
    // @var null|integer
    private $originalMemoryLimit = null;

    /**
     * Add function to be used in cron event.
     *
     * @see parent::init()
     */
    public function init()
    {
        // Action on cron
        $this->subscribe('direct', 'backupSurveys');
        // Needed config
        $this->subscribe('beforeActivate');

        // The survey seeting
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');

        // The menu
        $this->subscribe('beforeToolsMenuRender');
    }

    /**
     *.
     */
    public function beforeToolsMenuRender()
    {
        $menuEvent = $this->getEvent();
        $surveyId = $menuEvent->get('surveyId');
        // This need export whole right
        if (!$this->hasExportSurveyPermission($surveyId)) {
            return;
        }
        if (!$this->get('active', 'Survey', $surveyId, false)) {
            return;
        }
        $localDirectory = @realpath($localDirectory);
        if (false === $localDirectory) {
            return;
        }
        if (!@is_dir($localDirectory)) {
            return;
        }
        $aMenuItem = [
            'label' => $this->gT('Get previous backup'),
            'iconClass' => 'fa fa-upload',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                [
                    'sa' => 'sidebody',
                    // ~ 'href' => $url,
                    'plugin' => get_class($this),
                    'method' => 'actionList',
                    'surveyId' => $surveyId,
                ]
            ),
        ];
        if (class_exists('\\LimeSurvey\\Menu\\MenuItem')) {
            $menuItems[] = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        } else {
            $menuItems[] = new \ls\menu\MenuItem($aMenuItem);
        }
        $menuEvent->append('menuItems', $menuItems);
    }

    /**
     * List already done backup
     * and give link.
     *
     * @param mixed $surveyId
     */
    public function actionList($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        // This need export whole right
        if (!$this->hasExportSurveyPermission($surveyId)) {
            throw new CHttpException(403);
        }
        // Get the list
        $maxnum = intval($this->get('backupNumber'));
        $aSurveysBackup = [];
        $aSurveyBackup = self::getSurveyBackupInformation($surveyId, 'last');
        if (!empty($aSurveyBackup)) {
            $aSurveysBackup[] = $aSurveyBackup;
        }
        $count = 1;
        while ($count <= $maxnum) {
            $aSurveyBackup = self::getSurveyBackupInformation($surveyId, $count);
            if (!empty($aSurveyBackup)) {
                $aSurveysBackup[] = $aSurveyBackup;
            }
            $count++;
        }
        $aData['aSurveysBackup'] = $aSurveysBackup;
        $aData['translation'] = array(
            '%s in directory %s (%s)' => $this->translate('%s in directory %s at date %s'),
            'date unknow' => $this->translate('date unknow'),
            'No backup are done.' => $this->translate('No backup are done.'),
            'Survey structure (lss)' => $this->translate('Survey structure (lss)'),
            'Survey archive (.lsa)' => $this->translate('Survey archive (.lsa)'),
        );
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['title'] = $this->translate('Backup list of this survey');

        return $this->renderPartial('list', $aData, true);
    }


    /**
     * get the existing backup file for survey in directory
     * @param integer $sureyId
     * @return void
     */
    public function actionGet($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        // This need export whole right
        if (!$this->hasExportSurveyPermission($surveyId)) {
            throw new CHttpException(403);
        }
        $dir = App()->getRequest()->getQuery('dir');
        $dir = strval($dir);
        if (!ctype_digit($dir) && $dir != 'last') {
            throw new CHttpException(403, "Invalid directory");
        }
        $currentDir = $this->get('localDirectory') . DIRECTORY_SEPARATOR . 'backup-' . $dir . DIRECTORY_SEPARATOR;
        if (is_file($currentDir . "survey_{$surveyId}.lss")) {
            $filename = "survey_{$surveyId}.lss";
            header("Content-Type: text/xml; charset=UTF-8");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Expires: 0");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: must-revalidate, no-store, no-cache");
            readfile($currentDir . "survey_{$surveyId}.lss");
            App()->end();
        }
        if (is_file($currentDir . "survey_archive_{$surveyId}.lsa")) {
            $filename = "survey_archive_{$surveyId}.lsa";
            header("Content-Type: application/zip");
            header("Content-Disposition: attachment; filename={$filename}");
            header("Expires: 0");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: must-revalidate, no-store, no-cache");
            header("Content-Transfer-Encoding: binary");
            readfile($currentDir . "survey_archive_{$surveyId}.lsa");
            App()->end();
        }
        throw new CHttpException(404, "File not found");
    }
    /**
     * get survey archibe information
     * @param $surveyId the final directory
     * @param $dir the final directory
     * @return array
     */
    private function getSurveyBackupInformation($surveyId, $dir)
    {
        $localDirectory = $this->get('localDirectory');
        $timeadjust = App()->getConfig('timeadjust');
        $dir = strval($dir);
        if (!ctype_digit($dir) && $dir != 'last') {
            throw new CHttpException(500, "Invalid dir in backupSurveyLocally::getSurveyBackupInformation");
        }
        $currentDir = $localDirectory . DIRECTORY_SEPARATOR . 'backup-' . $dir;
        $aSurveyBackup = array();
        if (@is_dir($currentDir)) {
            $backupDir = $currentDir . DIRECTORY_SEPARATOR;
            if (is_file($backupDir . "survey_{$surveyId}.lss")) {
                $aSurveyBackup = [
                    'type' => 'lss',
                    'dir' => $dir,
                    'datetime' => null,
                ];
                $timeFile = @filectime($backupDir . "survey_{$surveyId}.lss");
                if ($timeFile) {
                    $aSurveyBackup['datetime'] = dateShift(date('Y-m-d H:i:s', $timeFile), 'Y-m-d H:i:s', $timeadjust);
                }
            }
            if (is_file($backupDir . "survey_archive_{$surveyId}.lsa")) {
                $aSurveyBackup = [
                    'type' => 'lsa',
                    'dir' => $dir,
                    'datetime' => null,
                ];
                $timeFile = @filectime($backupDir . "survey_archive_{$surveyId}.lsa");
                if ($timeFile) {
                    $aSurveyBackup['datetime'] = dateShift(date('Y-m-d H:i:s', $timeFile), 'Y-m-d H:i:s', $timeadjust);
                }
            }
        }
        return $aSurveyBackup;
    }

    /**
     * The real action of this plugin, find all survey to be saved
     * Renaming dir one by one
     * Save surveys as LSA in the #1.
     */
    public function backupSurveys()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->event->get('target') != get_class()) {
            return;
        }
        if (empty($this->get('localDirectory'))) {
            $this->backupSurveyLog("localDirectory is not set, unable to save your surveys.", 0, "backupSurveys");
            throw new CException(
                'localDirectory is not set, unable to save your survey‘s.'
            );
        }
        $this->ownLsCommandFix();

        $localDirectory = $this->ownCheckDirectory($this->get('localDirectory'));
        if (empty($localDirectory)) {
            $this->backupSurveyLog($this->_errorOnDir, 0, "backupSurveys");
            throw new CException($this->_errorOnDir);
        }
        $this->ownDirectoryRotate($localDirectory, $this->get('backupNumber'));

        $this->ownSetMemoryLimit();
        $this->ownDisableErrorReporting();
        // lsa is zip
        if (!defined('PCLZIP_TEMPORARY_DIR')) {
            define(
                'PCLZIP_TEMPORARY_DIR',
                $localDirectory .
                    DIRECTORY_SEPARATOR .
                    'tmp' .
                    DIRECTORY_SEPARATOR
            );
        }
        Yii::import('application.libraries.admin.pclzip', true);

        /* Backup from survey settings to Y*/
        $criteria = new CDbCriteria();
        $criteria->compare('plugin_id', $this->id);
        $criteria->compare(
            App()->getDb()->quoteColumnName('key'),
            'active'
        );
        $criteria->compare(
            App()->getDb()->quoteColumnName('value'),
            'Y',
            true // Partial (saved json encoded)
        );
        $oSurveysToSave = \PluginSetting::model()->findAll($criteria);
        $this->backupSurveyLog("Save " . count($oSurveysToSave) . " surveys by settings", 2, "backupSurveys");
        foreach ($oSurveysToSave as $oSurvey) {
            $surveyId = $oSurvey->model_id;
            $this->ownSaveSurvey($surveyId, $localDirectory);
        }

        /* Backup from txt file */
        if (is_file($localDirectory . DIRECTORY_SEPARATOR . 'surveys.txt')) {
            $surveystxt = file_get_contents(
                $localDirectory . DIRECTORY_SEPARATOR . 'surveys.txt'
            );
            $surveyList = preg_split(
                '/\n|\r/',
                $surveystxt,
                -1,
                PREG_SPLIT_NO_EMPTY
            );
            $this->backupSurveyLog("Save " . count($surveyList) . " surveys by local file", 2, "backupSurveys");
            foreach ($surveyList as $surveyId) {
                $this->ownSaveSurvey(trim($surveyId), $localDirectory);
            }
        }
        $this->ownResetServerSettings();
    }

    /** Add the settings */
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('survey');
        $this->getEvent()->set("surveysettings.{$this->id}", [
            'name' => get_class($this),
            'settings' => [
                'active' => [
                    'type' => 'select',
                    'label' => $this->translate('Backup survey locally.'),
                    'options' => [
                        'N' => $this->translate("No"),
                        'Y' => $this->translate("Yes"),
                    ],
                    'current' => $this->get(
                        'active',
                        'Survey',
                        $surveyId,
                        'N'
                    ),
                ],
            ],
        ]);
    }

    /** Save the settings */
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        foreach ($oEvent->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $oEvent->get('survey'), '');
        }
    }

    /**
     * Must set a directory before activate.
     */
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (empty($this->get('localDirectory'))) {
            $this->getEvent()->set(
                'message',
                $this->translate(
                    'This plugin must be configured before it can be activated.'
                )
            );
            $this->getEvent()->set('success', false);
            return;
        }
    }

    /**
     * @see parent::saveSettings()
     *
     * @param mixed $settings
     */
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        if (!empty($settings['localDirectory'])) {
            $checkLocalDirectory = $settings['localDirectory'];
            $settings['localDirectory'] = $this->ownCheckDirectory(
                $settings['localDirectory']
            );
            if (
                $settings['localDirectory']
                && $settings['localDirectory'] != $checkLocalDirectory
            ) {
                App()->setFlashMessage(
                    $this->translate(
                        'The directory was updated to the real path.'
                    ),
                    'warning'
                );
            }
            if (!$settings['localDirectory']) {
                App()->setFlashMessage($this->_errorOnDir, 'danger');
            }
        }
        parent::saveSettings($settings);
    }

    // Set default when show setting
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'read')) {
            throw new CHttpException(403);
        }
        $aPluginSettings = parent::getPluginSettings($getValues);
        $aPluginSettings['localDirectory']['help'] = CHtml::tag(
            'p',
            [],
            $this->translate(
                'This directory must be out of web access, but web user must be allowed to create directory and write in this directory.'
            )
        );
        $aPluginSettings['localDirectory']['help'] .= CHtml::tag(
            'p',
            [],
            sprintf(
                $this->translate('The directory of Limesurvey is : %s'),
                App()->getConfig('rootdir')
            )
        );
        $aPluginSettings['setMemoryLimit']['default'] = App()->getConfig('memory_limit');
        $aPluginSettings['setMemoryLimit']['htmlOptions'] = [
            'placeholder' => App()->getConfig('memory_limit'),
        ];

        if (
            $getValues
            && !empty($aPluginSettings['localDirectory']['current'])
        ) {
            $checkLocalDirectory =
                $aPluginSettings['localDirectory']['current'];
            $settings['localDirectory'] = $this->ownCheckDirectory(
                $aPluginSettings['localDirectory']['current']
            );
            if (
                $aPluginSettings['localDirectory']['current']
                && $aPluginSettings['localDirectory']['current'] !=
                    $checkLocalDirectory
            ) {
                App()->setFlashMessage(
                    $this->translate(
                        'The directory was updated to the real path.'
                    ),
                    'warning'
                );
            }
            if (!$aPluginSettings['localDirectory']['current']) {
                App()->setFlashMessage($this->_errorOnDir, 'danger');
            }
        }

        return $aPluginSettings;
    }

    /**
     * Check if a directory is valid for purpose.
     *
     * @param mixed $localDirectory
     *
     * @return false|string
     */
    private function ownCheckDirectory($localDirectory)
    {
        // Remove the warning about open_base_dir
        $localDirectory = @realpath($localDirectory);
        if (false === $localDirectory) {
            $this->_errorOnDir = $this->translate(
                'The directory you set is invalid and can not be used.'
            );

            return false;
        }
        rmdirr($localDirectory . '/backupSurveyLocallyCheck');
        if (!mkdir($localDirectory . '/backupSurveyLocallyCheck')) {
            $this->_errorOnDir = $this->translate(
                'Unable to create directory in directory set.'
            );

            return false;
        }
        // ~ if(!mkdir($localDirectory."/backupSurveyLocallyCheck/dir")) {
        // ~ $this->_errorOnDir = $this->translate("Able to create directory in directory set, but unable to create a directory in this new directory.");
        // ~ return false;
        // ~ }
        if (!touch($localDirectory . '/backupSurveyLocallyCheck/file')) {
            $this->_errorOnDir = $this->translate(
                'Able to create directory in directory set, but unable to create a file in the directory created.'
            );

            return false;
        }
        if (!rmdirr($localDirectory . '/backupSurveyLocallyCheck')) {
            $this->_errorOnDir = $this->translate(
                'Able to create directory in directory set, but unable to delete.'
            );

            return false;
        }
        if (!is_dir($localDirectory . DIRECTORY_SEPARATOR . 'tmp')) {
            mkdir($localDirectory . DIRECTORY_SEPARATOR . 'tmp');
        }

        return $localDirectory;
    }

    /**
     * log an event.
     *
     * @param string $sLog
     * @param int $state error type for Yii log (0: ERROR(error), 1 INFO (warning), 2 DEBUG (info), 3 TRACE (trace))
     * @param string $detail
     * retrun @void
     */
    private function backupSurveyLog($sLog, $state = 0, $detail = "general")
    {
        // Play with DEBUG : ERROR/LOG/DEBUG
        $sNow = date(DATE_ATOM);
        switch ($state) {
            case 0:
                $sLevel = 'error';
                $sLogLog = "[ERROR] {$sLog}";
                break;
            case 1:
                $sLevel = 'warning';
                $sLogLog = "[WARNING] {$sLog}";
                break;
            case 2:
                $sLevel = 'info';
                $sLogLog = "[INFO] {$sLog}";
                break;
            default:
                $sLevel = 'trace';
                $sLogLog = "[DEBUG] {$sLog}";
                break;
        }
        Yii::log($sLog, $sLevel, 'plugin.backupSurveyLog.' . $detail);
        echo $sLogLog . "\n";
    }
    /**
     * @see parent::gT for LimeSurvey 3.0
     *
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode  unescaped by default
     * @param string $sLanguage    use current language if is null
     *
     * @return string
     */
    private function translate(
        $sToTranslate,
        $sEscapeMode = 'unescaped',
        $sLanguage = null
    ) {
        if (is_callable($this, 'gT')) {
            return $this->gT($sToTranslate, $sEscapeMode, $sLanguage);
        }

        return $sToTranslate;
    }

    /**
     * Fix LimeSurvey command function and add own needed function.
     *
     * @todo : find way to control API
     * OR if another plugin already fix it
     */
    private function ownLsCommandFix()
    {
        // Bad autoloading in command
        if (!class_exists('DbStorage', false)) {
            include_once dirname(__FILE__) . '/DbStorage.php';
        }
        if (!class_exists('ClassFactory', false)) {
            Yii::import('application.helpers.ClassFactory');
        }

        ClassFactory::registerClass('Token_', 'Token');
        ClassFactory::registerClass('Response_', 'Response');

        // needed for rmdirr function
        Yii::import('application.helpers.common_helper', true);
        // Replace getGlobalSetting core function (never save global settings when load it)
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        Yii::import(get_class($this) . '.helpers.globalsettings_helper', true);
        // Needed function for export
        Yii::import('application.helpers.export_helper', true);
        Yii::import('application.helpers.expressions.em_manager_helper', true);

        $defaulttheme = Yii::app()->getConfig('defaulttheme');
        // Bad config set for rootdir
        if (
            !is_dir(
                Yii::app()->getConfig('standardthemerootdir') .
                    DIRECTORY_SEPARATOR .
                    $defaulttheme
            )
            && !is_dir(
                Yii::app()->getConfig('userthemerootdir') .
                    DIRECTORY_SEPARATOR .
                    $defaulttheme
            )
        ) {
            /*
             * if still broken : throw error : risqk of updating survey when load
             * @see : https://gitlab.com/SondagesPro/mailing/sendMailCron/-/commit/31165bf12201e3dc82b927df095a8f5221bb73d3
             */
            $this->backupSurveyLog("Unable to find default theme {$defaulttheme} in current theme directory", 0, "ownLsCommandFix");
            throw new CException(
                "Unable to find default theme {$defaulttheme} in current theme directory"
            );
        }
    }

    /**
     * Create the backup directory rotation.
     *
     * @param string $dirName directory name (must exist)*
     * @param int (up to 1 …)
     * @param mixed $numberOfDir
     */
    private function ownDirectoryRotate($dirName, $numberOfDir)
    {
        $currentNum = $numberOfDir;
        if ($numberOfDir > 1) {
            if (
                is_dir(
                    $dirName . DIRECTORY_SEPARATOR . 'backup-' . $numberOfDir
                )
            ) {
                rmdirr(
                    $dirName . DIRECTORY_SEPARATOR . 'backup-' . $numberOfDir
                );
            }
        }
        --$currentNum;
        while ($currentNum > 0) {
            if (
                is_dir($dirName . DIRECTORY_SEPARATOR . 'backup-' . $currentNum)
            ) {
                $newNum = $currentNum + 1;
                rename(
                    $dirName . DIRECTORY_SEPARATOR . 'backup-' . $currentNum,
                    $dirName . DIRECTORY_SEPARATOR . 'backup-' . $newNum
                );
            }
            --$currentNum;
        }
        if (
            $numberOfDir > 1
            && is_dir($dirName . DIRECTORY_SEPARATOR . 'backup-last')
        ) {
            rename(
                $dirName . DIRECTORY_SEPARATOR . 'backup-last',
                $dirName . DIRECTORY_SEPARATOR . 'backup-1'
            );
        }
        mkdir($dirName . DIRECTORY_SEPARATOR . 'backup-last');
        if ($this->get('setChmod', null, null, false)) {
            chmod($dirName . DIRECTORY_SEPARATOR . 'backup-last', 0777);
        }
        // Put a file with complete date
        $fileName = date('Y-m-d_His') . '.txt';
        touch(
            $dirName .
                DIRECTORY_SEPARATOR .
                'backup-last' .
                DIRECTORY_SEPARATOR .
                $fileName
        );
        if ($this->get('setChmod', null, null, false)) {
            chmod(
                $dirName .
                    DIRECTORY_SEPARATOR .
                    'backup-last' .
                    DIRECTORY_SEPARATOR .
                    $fileName,
                0666
            );
        }
    }

    /**
     * Save a survey as lsa in directory.
     *
     * @param int    $surveyId survey id
     * @param string $dirName  directory name (must exist)
     *
     * @return bool true is survey was saved
     */
    private function ownSaveSurvey($surveyId, $dirName)
    {
        $survey = Survey::model()->findByPk($surveyId);
        if (empty($survey)) {
            $this->backupSurveyLog("Invalid survey id {$surveyId}", 1, "ownSaveSurvey");
            return;
        }
        if ('Y' != $survey->active) {
            $this->backupSurveyLog("Save LSS for surveyid {$surveyId}", 3, "ownSaveSurvey");
            $sLSSFileName =
                $dirName .
                DIRECTORY_SEPARATOR .
                'backup-last' .
                DIRECTORY_SEPARATOR .
                'survey_' .
                $surveyId .
                '.lss';
            touch($sLSSFileName);
            file_put_contents($sLSSFileName, surveyGetXMLData($surveyId));
            if ($this->get('setChmod', null, null, false)) {
                chmod($sLSSFileName, 0666);
            }
            $this->backupSurveyLog("LSS for surveyid {$surveyId} saved", 2, "ownSaveSurvey");
            return;
        }
        $this->backupSurveyLog("Save LSA for surveyid {$surveyId}", 3, "ownSaveSurvey");
        // This part is copy /paste from export->_exportarchive core function
        $aSurveyInfo = getSurveyInfo($surveyId);

        $saveZIPFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'backup-last' .
            DIRECTORY_SEPARATOR .
            'survey_archive_' .
            $surveyId .
            '.lsa';
        $sLSSFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lss' .
            randomChars(30);
        $sLSRFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lsr' .
            randomChars(30);
        $sLSTFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lst' .
            randomChars(30);
        $sLSIFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lsi' .
            randomChars(30);

        Yii::import('application.libraries.admin.pclzip', true);
        $zip = new PclZip($saveZIPFileName);
        touch($sLSSFileName);
        file_put_contents($sLSSFileName, surveyGetXMLData($surveyId));
        $this->ownAddToZip($zip, $sLSSFileName, 'survey_' . $surveyId . '.lss');
        unlink($sLSSFileName);

        if ('Y' == $survey->active) {
            getXMLDataSingleTable(
                $surveyId,
                'survey_' . $surveyId,
                'Responses',
                'responses',
                $sLSRFileName,
                false
            );
            $this->ownAddToZip(
                $zip,
                $sLSRFileName,
                'survey_' . $surveyId . '_responses.lsr'
            );
            unlink($sLSRFileName);
        }

        if (tableExists('{{tokens_' . $surveyId . '}}')) {
            getXMLDataSingleTable(
                $surveyId,
                'tokens_' . $surveyId,
                'Tokens',
                'tokens',
                $sLSTFileName
            );
            $this->ownAddToZip(
                $zip,
                $sLSTFileName,
                'survey_' . $surveyId . '_tokens.lst'
            );
            unlink($sLSTFileName);
        }

        if (tableExists('{{survey_' . $surveyId . '_timings}}')) {
            getXMLDataSingleTable(
                $surveyId,
                'survey_' . $surveyId . '_timings',
                'Timings',
                'timings',
                $sLSIFileName
            );
            $this->ownAddToZip(
                $zip,
                $sLSIFileName,
                'survey_' . $surveyId . '_timings.lsi'
            );
            unlink($sLSIFileName);
        }
        if ($this->get('setChmod', null, null, false)) {
            chmod($saveZIPFileName, 0666);
        }
        $this->backupSurveyLog("LSA for surveyid {$surveyId} saved", 2, "ownSaveSurvey");
        return true;
    }

    /**
     * Setthe memory limit
     * @return void
     */
    private function ownSetMemoryLimit()
    {
        $memoryLimit = intval($this->get("setMemoryLimit"));
        if (empty($memoryLimit)) {
            $memoryLimit = intval(App()->getConfig("memory_limit"));
        }
        if (ini_get('memory_limit') != -1 && convertPHPSizeToBytes(ini_get("memory_limit")) < convertPHPSizeToBytes($memoryLimit . 'M')) {
            $this->originalMemoryLimit = ini_get('memory_limit');
            try {
                ini_set("memory_limit", $memoryLimit . 'M'); // Set Memory Limit for big surveys
            } catch (Exception $e) {
                $this->backupSurveyLog("Unable to set memory_limit to  {$memoryLimit}M", 0, "ownSetMemoryLimit");
            };
        }
    }

    /**
     * Disable error if needed
     * @return @void
     */
    private function ownDisableErrorReporting()
    {
        if (!$this->get('disableErrorReporting', null, null, true)) {
            return;
        }
        $this->originalErrorReporting = error_reporting();
        if (!defined('YII_DEBUG')) {
            define('YII_DEBUG', false);
        }
        error_reporting(0);
    }

    /**
     * Disbale error if needed
     * @return @void
     */
    private function ownResetServerSettings()
    {
        if (!is_null($this->originalErrorReporting)) {
            error_reporting($this->originalErrorReporting);
        }
        if (!is_null($this->originalMemoryLimit)) {
            ini_set("memory_limit", $this->originalMemoryLimit);
        }
    }


    /**
     * Function copy from export.
     *
     * @see \export->_addToZip
     *
     * @param PclZip $zip
     * @param string $name
     * @param string $full_name
     */
    private function ownAddToZip($zip, $name, $full_name)
    {
        $check = $zip->add([
            [
                PCLZIP_ATT_FILE_NAME => $name,
                PCLZIP_ATT_FILE_NEW_FULL_NAME => $full_name,
            ],
        ]);
        if (0 == $check) {
            if ($this->get('disableErrorReporting', null, null, true)) {
                $this->backupSurveyLog("PCLZip error : "  . $zip->errorInfo(true), 0, "ownAddToZip");
            } else {
                throw new CException('PCLZip error : ' . $zip->errorInfo(true));
            }
        }
    }

    /** Check if have sufficient permisison
     * Check whoe directly
     * @return boolena
     */
    private function hasExportSurveyPermission($surveyId)
    {
        return Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            && Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            && Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export');
    }
}
