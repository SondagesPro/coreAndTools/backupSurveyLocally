<?php

/*
 * Limesurvey core file replacement : lighter and remove uneeded part
 */

/**
 * Returns a global setting
 *
 * @param string $settingname
 * @return string
 */
function getGlobalSetting($settingname)
{
    return App()->getConfig($settingname);
}
