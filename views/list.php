<div class="row">
    <div class="col-lg-12 content-right">
        <?php if ($title) {
            echo CHtml::tag(
                "h3",
                array(
                    'id' => 'title-form-' . $pluginClass
                ),
                $title
            );
        } ?>
    </div>
    <?php if (empty($aSurveysBackup)) : ?>
        <p><?= $translation['No backup are done.']; ?></p>
    <?php else: ?>
        <ul>
            <?php foreach ($aSurveysBackup as $aSurveyBackup) : ?>
            <li>
                <?php
                if ($aSurveyBackup['type'] == 'lsa') {
                    $typetranslated = $translation['Survey archive (.lsa)'];
                } else {
                    $typetranslated = $translation['Survey structure (lss)'];
                }
                $datetime = $aSurveyBackup['datetime'];
                if (empty($datetime)) {
                    $datetime = $translation['date unknow'];
                }
                $urllabel = sprintf($translation['%s in directory %s (%s)'], $typetranslated, $aSurveyBackup['dir'], $datetime);
                echo Chtml::link(
                    $urllabel,
                    array(
                        'admin/pluginhelper',
                        'sa' => 'sidebody',
                        'plugin' => $pluginClass,
                        'method' => 'actionGet',
                        'surveyId' => $surveyId,
                        'dir' => $aSurveyBackup['dir']
                    )
                );
                ?>
            </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>

